//
//  CityViewController.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {
    
    var city: City?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var squareLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
    @IBOutlet weak var foundationYearLabel: UILabel!
    @IBOutlet weak var mayorLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "MuseumCell", bundle: nil), forCellReuseIdentifier: "MuseumCell")
        setup()
    }
    
    func setup() {
        imageView.image = UIImage(named: city?.photo ?? "")
        nameLabel.text = city?.name
        squareLabel.text = city?.square
        populationLabel.text = city?.population
        foundationYearLabel.text = city?.foundationYear
        mayorLabel.text = city?.mayor
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CityViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return city?.museums.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MuseumCell", for: indexPath) as! MuseumCell
        cell.update(museum: (city?.museums[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MuseumViewController") as! MuseumViewController
        vc.museum = city?.museums[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
