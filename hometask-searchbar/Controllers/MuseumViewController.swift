//
//  MuseumViewController.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class MuseumViewController: UIViewController {

    var museum: Museum?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var foundationYearLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    func setupUI() {
        imageView.image = UIImage(named: museum!.photo)
        imageView.alpha = 0.75
        nameLabel.text = museum?.name
        typeLabel.text = museum?.type
        foundationYearLabel.text = museum?.foundationYear
        priceLabel.text = museum?.price
        descriptionLabel.text = museum?.description
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
