//
//  SearchCityViewController.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class SearchCityViewController: UIViewController {

    var cities = CityManager()
    var filteredCities: [City] = []
    var filtered = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "CityCell", bundle: nil), forCellWithReuseIdentifier: "CityCell")
        searchBar.delegate = self
        collectionView.reloadData()
    }

    
}

extension SearchCityViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filtered {
            return filteredCities.count
        } else {
            return cities.array.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCell", for: indexPath) as! CityCell
        if filtered {
            cell.update(city: filteredCities[indexPath.row])
        } else {
            cell.update(city: cities.array[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "CityViewController") as! CityViewController
        if filtered {
            vc.city = filteredCities[indexPath.row]
        } else {
            vc.city = cities.array[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SearchCityViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2 {
            filtered = true
            filteredCities = cities.array.filter({$0.name.lowercased().contains(searchText.lowercased())})
            collectionView.reloadData()
        } else {
            filtered = false
            collectionView.reloadData()
        }
    }
}
