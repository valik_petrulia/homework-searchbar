//
//  MuseumCell.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class MuseumCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(museum: Museum) {
        nameLabel.text = museum.name
    }
    
}
