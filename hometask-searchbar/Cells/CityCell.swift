//
//  CityCell.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class CityCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(city: City) {
        nameLabel.text = city.name
        imgView.image = UIImage(named: city.photo)
        imgView.layer.cornerRadius = 10
        imgView.alpha = 0.75
    }

}
