//
//  MuseumModel.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class Museum {
    var photo: String
    var name: String
    var type: String
    var foundationYear: String
    var price: String
    var description: String
    
    init(name: String, photo: String, type: String, foundationYear: String, price: String, description: String) {
        self.name = name
        self.photo = photo
        self.foundationYear = foundationYear
        self.description = description
        self.type = type
        self.price = price
    }
}
