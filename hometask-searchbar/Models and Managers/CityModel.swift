//
//  CityModel.swift
//  hometask-searchbar
//
//  Created by Валентин Петруля on 8/5/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class City {
    var photo: String
    var name: String
    var foundationYear: String
    var population: String
    var square: String
    var mayor: String
    var museums: [Museum]
    
    init(name: String, photo: String, square: String, population: String, foundationYear: String, mayor: String, museums: [Museum]) {
        self.name = name
        self.photo = photo
        self.foundationYear = foundationYear
        self.square = square
        self.population = population
        self.mayor = mayor
        self.museums = museums
    }
}
